package main

import (
	"flag"
	"fmt"
	"github.com/go-vgo/robotgo"
	"math/rand"
	"time"
)

func countdown(delay int)  {
	for x := delay ; x > 0 ; x-- {
		fmt.Printf("%d   \r", x)
		time.Sleep(time.Duration(1000) * time.Millisecond)
	}
}

func randomNumber() int {
	for {
		rand.Seed(time.Now().UnixNano())
		ri := rand.Intn(120)
		if ri > 90 {
			return ri
		}
	}
}

func main() {
	c := *flag.Int("c", 500, "how many clicks")
	d := *flag.Int("d", 10, "countdown delay in seconds")
	flag.Parse()
	countdown(d)
	for x := 0 ; x < c; x++ {
		robotgo.MouseClick("left", true)
		time.Sleep(time.Duration(randomNumber())*time.Millisecond)
	}
}